import Vue from "vue";
import VueRouter from "vue-router";
import About from "../views/About.vue";
import Contact from "../views/Contact.vue";
import Signup from "../views/Signup.vue";
import Signin from "../views/Signin.vue";
import Home from "../views/Home.vue";
Vue.use(VueRouter);

const routes = [{
        path: "/about",
        name: "About",
        component: About,
    },
    {
        path: "/",
        name: "Home",
        component: Home,
    },
    {
        path: "/Contact",
        name: "Contact",
        component: Contact,
    },
    {
        path: "/Signup",
        name: "Signup",
        component: Signup,
    },
    {
        path: "/Signin",
        name: "Signin",
        component: Signin,
    }
];

const router = new VueRouter({
    routes
});

export default router;