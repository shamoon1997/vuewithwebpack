const path = require('path');
var HtmlWebpackPlugin = require('html-webpack-plugin');
const { VueLoaderPlugin } = require('vue-loader');
module.exports = {
    mode: "development",
    entry: "./src/main.js",
    output: {
        path: path.resolve(__dirname, "build"),
        filename: "build.js",
    },
    module: {
        rules: [{
                test: /\.vue$/,
                loader: 'vue-loader',

            },
            {
                test: /\.css$/i,
                use: ['style-loader', 'css-loader'],
            }

        ]

    },
    plugins: [
        new VueLoaderPlugin(),
        new HtmlWebpackPlugin(new HtmlWebpackPlugin),
    ]
};